﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DocNotes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuMaster : ContentPage
    {
        public ListView ListView;

        public MenuMaster()
        {
            InitializeComponent();

            BindingContext = new MenuMasterViewModel();
            ListView = MenuItemsListView;
        }

        class MenuMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MenuMenuItem> MenuItems { get; set; }

            public MenuMasterViewModel()
            {
                MenuItems = new ObservableCollection<MenuMenuItem>(new[]
                {
                    new MenuMenuItem { Id = 0, Title = "Профиль", },
                    new MenuMenuItem { Id = 1, Title = "Текущая программа лечения" },
                    new MenuMenuItem { Id = 2, Title = "Мои рецепты" },
                    new MenuMenuItem { Id = 3, Title = "Связь с врачом" },
                    new MenuMenuItem { Id = 4, Title = "Настройки", },
                    new MenuMenuItem { Id = 5, Title = "Выйти" },
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion

            ///private async void ToZeroPage(object sender, EventArgs e)
            ///{
            ///    await Navigation.PushAsync(new RegistationPage(true, null));
            ///}
        }

        public void Unlogin()
        {
            //Clear Person object
        }

        private async void ChoiceBtn_Clicked(object sender, EventArgs e)
        {
            Button choiceBtn = (Button)sender;
            switch (choiceBtn.Text)
            {
                case "Профиль":
                    await Navigation.PushAsync(new ProfilePage());
                    break;
                case "Настройки":
                    await Navigation.PushAsync(new SettingsPage());
                    break;
                case "Текущая программа лечения":
                    await Navigation.PushAsync(new MenuDetail());
                    break;
                case "Мои рецепты":
                    await Navigation.PushAsync(new RecipesPage());
                    break;
                case "Связь с врачом":
                    await Navigation.PushAsync(new DocContactPage());
                    break;
                case "Выйти":
                    Unlogin();
                    break;

                default:
                    await Navigation.PushAsync(new MenuDetail());
                    break;
            }
        }
    }
}