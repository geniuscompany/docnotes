﻿using System;
using System.Collections.Generic;
using Server;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DocNotes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistationPage : ContentPage
    {
        Person human = new Person(null, null, null, true, DateTime.Now, null, null, null);

        private void FillIn(Person person)
        {
            this.Name.Text = person.GetName();
            this.Surname.Text = person.GetSurname();
            this.Middle_Name.Text = person.GetMiddlename();
            this.Birthday.Date = person.GetBirth();
            this.Polis.Text = person.GetPolis();
            this.Mail.Text = person.GetMail();
            IList<string> Sex = new List<string>();
            Sex.Add("Мужской");
            Sex.Add("Женский");
            if (person.GetSex())
            {
                this.Sex.SelectedItem = Sex[0];
            }
            else
            {
                this.Sex.SelectedItem = Sex[1];
            }

            human = person;
        }

        public RegistationPage(bool change_data, Person person)
        {
            InitializeComponent();
            if (change_data)
            {
                FillIn(person);
            }
            Sex.Items.Add("Мужской");
            Sex.Items.Add("Женский");
            Sex.SelectedIndex = 0;
        }

        private int CheckRegistration()
        {
            if (Password.Text != Retry_Password.Text)
            {
                return 1;
            }
            if (Password.Text.Length < 6)
            {
                return 2;
            }
            //if (Server.CheckLogin(Mail.Text))
            //    return 3;
            return 0;
        }


        public async void IfCorrent()
        {
            bool male = (Sex.SelectedIndex == 0);
            Person human1 = new Person(Surname.Text, Name.Text, Middle_Name.Text,
                                        male, Birthday.Date, Polis.Text, Mail.Text, Password.Text);
            human = human1;
            CreateUser.Create(human);
            await Navigation.PushAsync(new Menu());
        }

        private async void Registration_Button_Clicked(object sender, EventArgs e)
        {
            switch (CheckRegistration())
            {
                case 1:
                    {
                        await DisplayAlert("Несовпадение", "Введенные пароли не совпадают", "Назад");
                    }
                    break;
                case 2:
                    {
                        await DisplayAlert("Опасность", "Слабый пароль", "Исправить");
                    }
                    break;
                case 0:
                    IfCorrent();
                    break;
                default:
                    IfCorrent();
                    break;
            }
        }
    }
}