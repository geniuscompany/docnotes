﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DocNotes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();
        }

        ///Переход на страницу регистрации и вызов функции FillIn(Person person) в методе RegistationPage()
        private async void Change_Data_Button_Clicked(object sender, EventArgs e)
        {
            //Вместо null в вызове функции необходимо отправлять класс пользователя из базы данных
            await Navigation.PushAsync(new RegistationPage(true, null));
        }
    }
}