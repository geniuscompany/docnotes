﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DocNotes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuDetail : ContentPage
    {
        public MenuDetail()
        {
            InitializeComponent();
        }
    }
}