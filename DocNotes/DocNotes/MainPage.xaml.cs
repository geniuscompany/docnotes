﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DocNotes
{
    public partial class MainPage : ContentPage
    {
        private void SetupColors()
        {
            /* mainstack.BackgroundColor = Color.Black;
             regButton.BackgroundColor = Color.FromHex("#2F4F4F");
             entryButton.BackgroundColor = Color.FromHex("#2F4F4F");
             welclbl.TextColor = Color.FromHex("#00FF7F");*/
        }
        public MainPage()
        {
            InitializeComponent();
            SetupColors();
        }

        private async void Entry(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EntryPage());
        }

        private async void Registration(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RegistationPage(false, null));
        }
    }
}
