﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.LocalNotifications;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Server;

namespace DocNotes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntryPage : ContentPage
    {
        public EntryPage()
        {
            InitializeComponent();

        }
        private async void Enter(object sender, EventArgs e)
        {
            Menu mainMenu = new Menu();
            NavigationPage.SetHasBackButton(mainMenu, false);
            // var char[] email = new char[256];
            
            
                
                    
                        if (SecondTry.AuthorizeUser(Mail.Text, Pass.Text))
                        {
                            CrossLocalNotifications.Current.Show("Hello", "body", 443);
                            await Navigation.PushAsync(mainMenu);
                            var existingPages = Navigation.NavigationStack.ToList();
                            foreach (var page in existingPages)
                            {
                                Navigation.RemovePage(page);
                            }
                        }
                        else
                            await DisplayAlert("Ошибка", "Введен неверный пароль или почта", "Назад");
                    }
                   
                
                   
            }
        }
    